/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function inputUserInfo() {
		let fullName = prompt('Enter your full name');
		let age = prompt('Enter your age');
		let location = prompt('Where are you from?');

		console.log('Hi! ' + fullName);
		console.log(fullName + "\'s age is " + age);
		console.log(fullName + ' is located at ' + location);
	};

	inputUserInfo();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function topFiveBands(){
		let bands = ['ITZY', 'Redvelvet', 'TWICE', 'BTOB', 'STAYC'];
		let top1Band = 'ITZY';
		let top2Band = 'Redvelvet';
		let top3Band = 'TWICE';
		let top4Band = 'BTOB'
		let top5Band = 'STAYC';
		

		console.log('1. ' + top1Band);
		console.log('2. ' + top2Band);
		console.log('3. ' + top3Band);
		console.log('4. ' + top4Band);
		console.log('5. ' + top5Band);
	}

	topFiveBands();



/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function topFiveMovies() {
		let top1Movie = 'Avengers';
		let tomatoMeter1 = '91%';
		let top2Movie = 'Avengers Infinity War';
		let tomatoMeter2 = '85%';
		let top3Movie = 'Avengers End Game';
		let tomatoMeter3 = '94%';
		let top4Movie = 'Multiverse of Madness';
		let tomatoMeter4 = '74%';
		let top5Movie = 'Iron Man';
		let tomatoMeter5 = '94%';

		console.log('1. ' + top1Movie);
		console.log('Tomatometer for ' + top1Movie + ': ' + tomatoMeter1);
		console.log('2. ' + top2Movie);
		console.log('Tomatometer for ' + top2Movie + ': ' + tomatoMeter2);
		console.log('3. ' + top3Movie);
		console.log('Tomatometer for ' + top3Movie + ': ' + tomatoMeter3);
		console.log('4. ' + top4Movie);
		console.log('Tomatometer for ' + top4Movie + ': ' + tomatoMeter4);
		console.log('5. ' + top5Movie);
		console.log('Tomatometer for ' + top5Movie + ': ' + tomatoMeter5);
	};

	topFiveMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();

